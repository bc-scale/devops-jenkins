pipeline {
    agent any
    tools {
        maven "maven-3.6"
    }
    stages {
        stage("increment version") {
            steps {
                script {
                    echo "Incrementing version..."
                    sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion}\
                    versions:commit'
                    def matcher = readFile("pom.xml") =~ "<version>(.+)</version>"
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('build app') {
            steps {
                script {
                    echo "Building jar file..."
                    sh "mvn clean package"
                }
            }
        }
        stage('build image') {
            steps {
                script {
                    echo "Build and push docker image..."
                    withCredentials([usernamePassword(credentialsId: "dockerhub-repo", passwordVariable: "PASSWORD", usernameVariable: "USERNAME")]) {
                    sh "docker build -t jimsemara/java-maven-app:${IMAGE_NAME} ."
                    sh "echo $PASSWORD | docker login -u $USERNAME --password-stdin"
                    sh "docker push jimsemara/java-maven-app:${IMAGE_NAME}"
                    }
                }
            }
        }
        stage('deploy') {
            steps {
                script {
                    echo "Deploying the application..."
                }
            }
        }
        stage('commit version update'){
            steps {
                script {
                    echo "Commit and push version update to git..."
                    withCredentials([usernamePassword(credentialsId: "dockerhub-repo", passwordVariable: "PASSWORD", usernameVariable: "USERNAME")]) {
                        sh 'git config --global user.email "jenkins@jenkins.com"'
                        sh 'git config --global user.name "jenkins"'
                        sh 'git config --global user.email jenkins@jenkins.com'
                        
                        sh 'git status'
                        sh 'git branch'
                        sh 'git config --list'
                        
                        sh 'git remote set-url origin https://${USERNAME}:${PASSWORD}@gitlab.com/jimsemara/devops-jenkins.git'
                        sh 'git add .'
                        sh 'git commit -m "Jenkins: Version bump."'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}